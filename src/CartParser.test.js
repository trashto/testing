import CartParser from './CartParser';
import path from 'path';
import { rest } from 'lodash';
import { expect, it } from '@jest/globals';


const cartPath = path.join(__dirname, '../samples/cart.csv');
const testPath = path.join(__dirname, '../samples/test.csv');
const badFormatPath = path.join(__dirname, '../samples/bad.csv');

let
	parser,
	parse,
	validate,
	parseLine,
	calcTotal;

beforeEach(() => {
	parser = new CartParser();
	parse = parser.parse.bind(parser);
	validate = parser.validate.bind(parser);
	parseLine = parser.parseLine.bind(parser);
	calcTotal = parser.calcTotal;

});

describe('CartParser - unit tests', () => {

	describe('validate', () => {
		it('should return empty array with no errors when contents is valid', () => {
			expect(validate('Product name,Price,Quantity')).toEqual([]);
		});

		it('should return "header" Error', () => {
			const content = 'name,Price,Quantity'

			const errors = validate(content);

			expect(errors.length).toEqual(1);
			const error = errors[0];
			expect(error.type).toEqual('header');
			expect(error.row).toEqual(0);
			expect(error.column).toEqual(0);
			expect(error.message).toEqual('Expected header to be named \"Product name\" but received name.');

		});

		it('should return "row" type of Error when cells less than 3', () => {
			const content = `Product name,Price,Quantity
				toy,6`;

			const errors = validate(content);

			expect(errors.length).toEqual(1);
			const error = errors[0];
			expect(error.type).toEqual('row');
			expect(error.row).toEqual(1);
			expect(error.column).toEqual(-1);
			expect(error.message).toEqual('Expected row to have 3 cells but received 2.');
		});

		it('should return "cell" type of Error when first cell is empty', () => {
			const content = `Product name,Price,Quantity,
				,3,2`;

			const errors = validate(content);

			expect(errors.length).toEqual(1);
			const error = errors[0];
			expect(error.type).toEqual('cell');
			expect(error.row).toEqual(1);
			expect(error.column).toEqual(0);
			expect(error.message).toEqual('Expected cell to be a nonempty string but received "".');
		});

		it('should return "cell" type of Error when price and quantity are not positive ints', () => {
			const content = `Product name,Price,Quantity,
				3,2,s`;

			const errors = validate(content)

			expect(errors.length).toEqual(1);
			const error = errors[0];
			expect(error.type).toEqual('cell');
			expect(error.row).toEqual(1);
			expect(error.column).toEqual(2);
			expect(error.message).toEqual('Expected cell to be a positive number but received "s".');
		});
	});

	describe('parseLine', () => {
		it('should return object with keys from column keys and values', () => {
			const line = 'Something,3,1';

			const item = parseLine(line);

			expect(item.id).toBeDefined();
			expect(item.price).toEqual(3);
			expect(item.name).toEqual('Something');
			expect(item.quantity).toEqual(1);
		});
	});

	describe('calcTotal', () => {
		it('should return 0 when quantity is 0', () => {
			const item1 = {
				name: 'Cookies',
				price: 5,
				quantity: 0
			};
			const item2 = {
				name: 'Candy',
				price: 10,
				quantity: 0
			};
			const items = [item1, item2];

			expect(calcTotal(items)).toEqual(0);
		});

		it('should return 0 when price is 0', () => {
			const item1 = {
				name: 'Cookies',
				price: 0,
				quantity: 2
			};
			const item2 = {
				name: 'Candy',
				price: 0,
				quantity: 4
			};
			const items = [item1, item2];

			expect(calcTotal(items)).toEqual(0);
		});

		it('should return 20 when price=5, quantity=4', () => {
			const item1 = {
				name: 'Candy',
				price: 5,
				quantity: 4
			};
			const items = [item1];

			expect(calcTotal(items)).toEqual(20);
		});
	});
});



describe('CartParser - integration test', () => {
	it('shoud return valid cart from new csv file', () => {
		const parser = new CartParser();
		const obj = parser.parse(testPath)

		expect(obj).toBeDefined();
		expect(obj.items).toBeDefined();
		expect(obj.items[5].name).toEqual('Rick');
		expect(obj.items[6].price).toEqual(14.90);
		expect(obj.items[7].quantity).toEqual(10);
		expect(obj.items.length).toEqual(9);

		expect(obj.total).toBeDefined();
	});

	it('shoud trow a validation error', () => {
		const parser = new CartParser();

		expect(() => parser.parse(badFormatPath)).toThrow(Error)
	});
});